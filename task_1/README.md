# Измерение размера TLB кэша

## Теоретическая часть

При трансляции линейного адреса в физический используется TLB для того,

чтобы хранить вычисленные физические адреса для наиболее часто используемых

страниц. Размер этого кэша фиксирован и варьируется в зависимости от модели

процессора. Т.к. поиск в кэше значительно быстрее, чем обход таблицы страниц,

используя различные размеры рабочего набора и паттернов доступа к данным,

по изменению времени выполнения программы можно определить размер кэша.

## План работы

Наиболее быстро программа будет работать, если все страницы с данными

закэшированы в TLB, и очень медленно когда каждое новое обращение требует 

полностью обходить таблицу страниц. Для того чтобы определить этот порог

нужно всего лишь измерить среднее время обращения к странице при последовательном

обращении к множеству страниц, после чего построить график зависимости времени

обращения от количества страниц - пороги, на которых будет переполняться TLB,

должны распознаваться на графике в виде ступенек, т.е. резкого повышения времени

обращения.


Для проведения такого эксперимента была написана программа на C++, которая

в логарифмическом порядке перебирает размеры массивов, для каждого размера

_REPEATS\_COUNT=100_ раз создает соответствующий массив, читает его (т.е.

заполняет TLB), после чего _READS\_COUNT=8_ раз его читает. Обращение к массивам

происходит с шагом 4096 (PAGE\_SIZE в моей операционной системе). Весь описанный

процесс происходит _EXPERIMENT\_COUNT=5_ раз. Для каждого из экспериментов

в соответствующий файл в папке build записывается среднее время обращения по

размерам массивов.


Для построения графика также написана программа на Python, которая читает

данные экспериментов и выводит графики зависимости среднего времени обращения

от размера массива для всех экспериментов.

### Компиляция и запуск тестовой программы, построение графика экспериментов

```sh

./run.sh

```

## Результаты

Измерения проводились для процессора _Intel(R) Core(TM) i5-8250U CPU @ 1.80GHz_

Таблицы с результатами измерений для каждого эксперимента находятся в директории data.

## Анализ результатов

При завершении программа вывела следующий график:

[![график time(size)](data/overview.png)](data/overview.png)

Здесь видно, что минимальное время обращения соответствует случаю, когда

происходит чтение из единственной страницы (на графике - до x=12), кроме

выброса в самом начале для одного из экспериментов. Рассмотрим только

интересующую нас часть этого графика:

[![график time(size)](data/window.png)](data/window.png)

Из графика легко увидеть видеть, что резкий рост среднего времени обращения

соответствует двум длинам массивов - 2^18 и 2^23 байт. Т.к. длина TLB

измеряется в страницах, то длины нужно резделить на 4096=2^12 байт. Получаем,

что рост среднего времени досутпа соответствует длинам массивов 2^6=64 и

2^11=2048. Если программа на данном размере массива достигла "ступеньки",

значит, *на этом шаге мы достигли переполнения TLB одного из уровней*.

Соответственно, получаем оценку для двух имеющихся в процессоре

TLB кэшей:

**33-64 страниц для TLB/DTLB кэша первого уровня** 

**1025-2048 страниц для TLB/DTLB кэша второго уровня** 

## Сравнение со спецификацией

Согласно информации из wikichip.org, в архитектуре Kaby Lake для

размера страницы 4Кб, имеются следующие TLB для данных:

* DTLB - 64 entries, 4-way set associative
* STLB - 1536 entries, 12-way set associative

что согласуется с экспериментальными данными.
