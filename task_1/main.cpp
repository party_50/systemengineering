#include <chrono>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

const int MAX_ARRAY_LEN = 1 << 30; // 24;
const int REPEATS_COUNT = 100;
const int READS_COUNT = 8;
const int STEP = 4096;
const int EXPERIMENTS_COUNT = 5;

static vector<char *> allocated;

double exp(int len) {
  using std::chrono::duration_cast;
  using std::chrono::high_resolution_clock;
  using std::chrono::nanoseconds;

  double access_time = 0;
  for (int j = 0; j < REPEATS_COUNT; ++j) {
    allocated.push_back(new char[8096]);
    char *data = new char[len];
    allocated.push_back(data);
    char temp = 0;

    // Accessing memory - push addresses to TLB
    for (int i = 0; i < len; i += STEP) {
      temp = data[i];
    }

    // Read them again
    double diff = 0;
    for (int k = 0; k < READS_COUNT; ++k) {
      for (int i = 0; i < len; i += STEP) {
        auto t1 = high_resolution_clock::now();
        temp = data[i];
        auto t2 = high_resolution_clock::now();
        diff += duration_cast<nanoseconds>(t2 - t1).count();
      }
    }

    access_time +=
        diff / static_cast<double>(READS_COUNT) / ((len - 1) / STEP + 1);
  }

  access_time /= REPEATS_COUNT;
  return access_time;
}

int main() {
  int temp = 0;
  // Experiments showed that at the very start of the program access time is
  // steadily higher. This "magic" loop gives a processor some work for a
  // second, it's enough to have stable result in the experiments. I suppose
  // it's cause of the process priority or something related.
  for (int i = 0; i < (1 << 30); ++i) {
    temp = i;
  }
  for (int j = 0; j < EXPERIMENTS_COUNT; ++j) {
    std::cout << "Experiment #" << j << std::endl;
    ofstream out;
    out.open("experiment_" + to_string(j) + ".txt");
    for (int i = 1; i < MAX_ARRAY_LEN; i *= 2) {
      double access_time = exp(i);
      out << access_time << '\n';
      std::cout << "\tLength = " << i << "\t Access time = " << access_time
                << std::endl;
    }
    out.close();
  }

  for (auto data : allocated) {
    delete[] data;
  }

  return 0;
}
