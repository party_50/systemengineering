import os
import matplotlib.pyplot as plt


def exp_name(exp_num):
    return 'experiment_' + str(exp_num) + '.txt'


print('Reading experiment data')

results = []
exp_num = 0
while os.path.exists(exp_name(exp_num)):
    print('Found experiment #' + str(exp_num))
    with open(exp_name(exp_num)) as fin:
        result = fin.read().split('\n')
        results.append(list(map(float, filter(lambda s: s != '', result))))
    exp_num += 1

if exp_num == 0:
    print('No experiments found')
    exit(0)

for i, r in enumerate(results):
    plt.plot(r, label='Exp #' + str(i))

print('Showing result')
plt.yscale('log')
plt.xlabel('log(len of array)')
plt.ylabel('average access time (ns)')
plt.legend()
plt.grid()
plt.show()
